# mdBook Docker image

Preloaded with the following mdBook extensions:

- Admonish
- PDF
- TOC

## Usage

The project needs to be setup accordingly to the instructions for mdBook, but also for the extensions mentioned
beforehand.

In the CI/CD pipeline:

```yaml
image: registry.gitlab.com/marc-andre/mdbook-docker-image

pages:
  stage: deploy
  script:
    - mdbook build $([ "$CI_COMMIT_BRANCH" = "main" ] && echo "-d public")
  artifacts:
    paths:
      - public
      - book

```