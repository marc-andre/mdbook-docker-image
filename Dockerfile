FROM rust:slim
MAINTAINER Marc-André Appel <mail@marc-andre.cc>
RUN apt update &&\
    apt install -y chromium pkg-config libssl-dev &&\
    cargo install mdbook mdbook-admonish mdbook-toc mdbook-pdf &&\
    apt purge -y pkg-config libssl-dev &&\
    apt autoremove -y &&\
    apt clean -y
